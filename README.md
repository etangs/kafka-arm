# kafka-arm

Minimalist kafka stack for demo or "Hello World !" in **kube arm**.

[Get all image tags](https://gitlab.com/etangs/kafka-arm/container_registry/4457437)

# How to use 

Deploy in docker

```
docker run -d -p 9092:9092 -p 2181:2181 --env KAFKA_SERVER_URL=0.0.0.0 registry.gitlab.com/etangs/kafka-arm:latest
```

Deploy in kube
```
helm install kafka-arm deploy
```