#FROM arm64v8/openjdk:11-jre
FROM openjdk:22-slim
#RUN wget https://dlcdn.apache.org/kafka/3.5.0/FAKE_VERSION.tgz
# RUN tar -zxf FAKE_VERSION.tgz -C /
# RUN rm FAKE_VERSION.tgz
ADD FAKE_VERSION /FAKE_VERSION
WORKDIR /FAKE_VERSION
ADD start.sh /FAKE_VERSION/start.sh
EXPOSE 2181 9092
CMD ["sh", "start.sh"]